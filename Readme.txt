﻿Core Navigator
Description: 
Software Engineering: Fall 2015
Navigation based mobile application intended for use by new Drury students. 
The app will have general information useful to new students and several features that will utilize the student's schedule. 

Most documentation exists in the wiki, some exists in the downloads page.

Team Members:
Dr. Sigman - Psuedo-CEO of Σ(n)
Kurt Smith - Project Manager
Barrett Cummins - Team Manager
Mark Ma - Technical Leader